using UnityEngine;
using DG.Tweening;

public class PlayerDeathAnimation : MonoBehaviour
{
	[SerializeField]
	private SpriteRenderer substitude;
    [SerializeField]
    private GameObject[] particles;

	private float oriScale = 0.5f;
    private float animationDuration = 0.5f;
	private float changeColourDuration = 1.5f;
	private Color red = Color.red;
	private Color white = Color.white;

    private Vector2[] scatterPostion = new Vector2[] { new Vector2(2.3f, 5), new Vector2(7.1f, 1.2f), new Vector2(3.73f, -4.68f), new Vector2(-2.22f, -3.43f), new Vector2(-5.32f, 1.58f), new Vector2(-2.68f, 6)};

	private void Start()
	{
		gameObject.SetActive(false);
	}

	public void PlayNormalDeathAnimation(Vector3 playerPos)
	{
		gameObject.transform.position = new Vector3(playerPos.x, playerPos.y);
		gameObject.SetActive(true);
		SpreadParticleAnimation();
	}

	public void PlayEmotionDeathAnimation(Vector3 playerPos)
	{
		gameObject.transform.position = new Vector3(playerPos.x, playerPos.y);
		gameObject.SetActive(true);
		substitude.gameObject.SetActive(true);
		substitude.color = white;
		substitude.DOColor(red, changeColourDuration).OnComplete(
			() => {
				substitude.gameObject.SetActive(false);
				SpreadParticleAnimation(red); 
			}
		);
	}

	private void SpreadParticleAnimation(Color? c = null)
	{
		for (int i = 0; i < particles.Length; i++)
		{
			particles[i].SetActive(true);
			if(c != null)
			{
				particles[i].GetComponent<SpriteRenderer>().color = (Color)c;
			}
			else
			{
				particles[i].GetComponent<SpriteRenderer>().color = white;
			}
			particles[i].transform.localPosition = new Vector3(0, 0, 0);
			particles[i].transform.localScale = new Vector3(oriScale, oriScale);
			particles[i].transform.DOLocalMove(scatterPostion[i], animationDuration);
			if (i == particles.Length - 1)
			{
				particles[i].transform.DOScale(0, animationDuration).OnComplete(() => { gameObject.SetActive(false); }); ;
			}
			else
			{
				particles[i].transform.DOScale(0, animationDuration);
			}
		}
	}
}
