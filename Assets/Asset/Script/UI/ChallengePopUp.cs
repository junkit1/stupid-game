using System.Collections;
using TMPro;
using UnityEngine;

public class ChallengePopUp : BaseUI
{
	[SerializeField]
	private TMP_Text deathReasonDesc;
	[SerializeField]
    private TMP_Text challengeText;
	[SerializeField]
	private TMP_Text countDownText;

    private const int minAmount = 5;
    private const int maxAmount = 10;
	private int countDownDuration = 0;
	private Coroutine countDownCoroutine;

	private void Start()
	{
		Hide();
	}

	public void ProcessDeathReason(DeathReason d)
	{
		deathReasonDesc.text = d == DeathReason.Emotion ? "Death by Emotional Damage!!!" : "Smashed to wall~~~";
	}

	public override void Show()
	{
        challengeText.text = $"Do <color=red><b>{Random.Range(minAmount, maxAmount)}</b></color> push-ups in ...";
		base.Show();
		StartCountDown();
	}

    void StartCountDown()
	{
		countDownDuration = StupidGameSetting.AdsCountDown;
		countDownText.text = countDownDuration.ToString();
		if(countDownCoroutine != null)
		{
			StopCoroutine(countDownCoroutine);
		}
		countDownCoroutine = StartCoroutine(TriggerCountDown());
	}

	void CheckCountDownRemaining()
	{
		if(countDownDuration > 1)
		{
			countDownDuration--;
			countDownText.text = countDownDuration.ToString();
			if (countDownCoroutine != null)
			{
				StopCoroutine(countDownCoroutine);
			}
			countDownCoroutine = StartCoroutine(TriggerCountDown());
		}
		else
		{
			StupidServiceLocator.AdsManager.ShowAd();
			StupidServiceLocator.UIManager.HideGamePlayUI();
			Hide();
		}
	}

	IEnumerator TriggerCountDown()
	{
		yield return new WaitForSeconds(1);
		CheckCountDownRemaining();
	}
}
