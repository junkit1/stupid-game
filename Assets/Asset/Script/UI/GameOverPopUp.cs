using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPopUp : BaseUI
{
    [SerializeField]
    private TMP_Text score;

    [SerializeField]
    private TMP_Text description;

    [SerializeField]
    private Button retryButton;

    [SerializeField]
    private Button mainMenuButton;

    private string[] descriptionTextArray = { "git gud", "lezduit again", "keep trying", "dun give up"};

	private void Awake()
	{
        retryButton.onClick.AddListener(OnRetry);
        mainMenuButton.onClick.AddListener(OnMainMenu);
	}

	private void Start()
	{
        Hide();
	}

	public override void Show()
	{
        score.text = $"{StupidServiceLocator.GameManager.Score}";
        description.text = descriptionTextArray[UnityEngine.Random.Range(0, descriptionTextArray.Length)];
        base.Show();
    }

    private void OnRetry()
	{
        Hide();
        StupidServiceLocator.GameManager.TriggerRestartEvent();
	}

    private void OnMainMenu()
	{
        Hide();
        StupidServiceLocator.GameManager.TriggerResetEvent();
        StupidServiceLocator.UIManager.ShowMenu();
	}
}
