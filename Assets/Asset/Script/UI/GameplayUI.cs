using UnityEngine;
using TMPro;
using System;

public class GameplayUI : BaseUI
{
    [SerializeField]
    private TMP_Text score;

    [SerializeField]
    private Joystick joystick;

    public Joystick Joystick => joystick;

    // Start is called before the first frame update
    void Start()
    {
        score.text = "0";
        StupidServiceLocator.GameManager.SubscribeRestartGameEvent(OnRestart);
        StupidServiceLocator.GameManager.SubscribeResetEvent(OnReset);
        Hide();
    }

    private void OnReset(object o, EventArgs e)
    {
        Reset();
    }

    private void OnRestart(object o, EventArgs e)
    {
        Reset();
    }

	public void UpdateScore(int _score)
	{
        score.text = _score.ToString();
	}

	public override void Hide()
	{
        joystick.OnPointerUp(null);
        base.Hide();
	}

    private void Reset()
    {
        score.text = "0";
    }
}
