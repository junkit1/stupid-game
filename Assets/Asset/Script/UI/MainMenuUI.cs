using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuUI : BaseUI
{
    [SerializeField]
    private TMP_Text highScore;

    [SerializeField]
    private Button playButton;

    [SerializeField]
    private Button exitButton;

    // Start is called before the first frame update
    void Start()
    {
        playButton.onClick.AddListener(OnPlay);
        exitButton.onClick.AddListener(OnExit);
        highScore.text = StupidServiceLocator.GameManager.HighScore.ToString();
    }

    void OnPlay()
	{
        Hide();
        StupidServiceLocator.GameManager.TriggerStartGame();
    }

    void OnExit()
	{
        Application.Quit();
	}

    public override void Show()
	{
        highScore.text = StupidServiceLocator.GameManager.HighScore.ToString();
        base.Show();
	}
}
