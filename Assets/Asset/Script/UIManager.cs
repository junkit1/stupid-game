using System;
using UnityEngine;

public class UIManager : MonoBehaviour
{
	[SerializeField]
	private MainMenuUI menu;
    [SerializeField]
    private GameplayUI gameplayUI;
	[SerializeField]
	private GameOverPopUp gameOverPopUp;
	[SerializeField]
	private ChallengePopUp challengePopUp;

	public Joystick Joystick => gameplayUI.Joystick;

	private void Awake()
	{
		StupidServiceLocator.RegisterUIManager(this);
	}

	private void Start()
	{
		StupidServiceLocator.GameManager.SubscribeStartGameEvent(OnStartGame);
		StupidServiceLocator.GameManager.SubscribeRestartGameEvent(OnRestart);
		StupidServiceLocator.GameManager.SubscribeGameOverEvent(OnGameOver);
	}

	private void OnStartGame(object o, EventArgs e)
	{
		gameplayUI.Show();
	}

	private void OnRestart(object o, EventArgs e)
	{
		gameplayUI.Show();
	}

	public void UpdateScoreUI(int s)
	{
        gameplayUI.UpdateScore(s);
	}

	public void HideGamePlayUI()
	{
		gameplayUI.Hide();
	}

	public void ShowGameOverPopUp()
	{
		gameOverPopUp.Show();
	}

	public void ShowChallengePopUp(DeathEventArgs e)
	{
		challengePopUp.ProcessDeathReason(e.deathReason);
		challengePopUp.Show();
	}

	public void ShowMenu()
	{
		menu.Show();
	}

	private void OnGameOver(object o, DeathEventArgs e)
	{
		if (StupidServiceLocator.AdsManager.AdsLoaded)
		{
			StupidServiceLocator.UIManager.ShowChallengePopUp(e);
		}
		else
		{
			StupidServiceLocator.UIManager.ShowGameOverPopUp();
		}
	}
}
