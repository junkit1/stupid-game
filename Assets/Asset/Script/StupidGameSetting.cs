
using UnityEngine;

public static class StupidGameSetting {

	private static readonly float oriObsTravelDuration = 5;
	private static readonly float minObsTravelDuration = 1f;
	private static readonly float obsTravelDurationDecrement = 0.5f;
	private static readonly int easyObsCount = 5;
	private static readonly int hardObsCount = 15;
	private static readonly float easyEnhancementChance = 0.3f;
	private static readonly float hardEnhancementChance = 0.7f;
	private static readonly int adsCountdown = 3;
	private static readonly float emotionalDeathDuration = 2f;

	public static float ObstacleTravelDuration = oriObsTravelDuration;

	public static float MinObsTravelDuration = minObsTravelDuration;

	public static float ObsTravelDurationDecrement = obsTravelDurationDecrement;

	public static float EasyEnhancementChance => easyEnhancementChance;

	public static float HardEnhancementChance => hardEnhancementChance;


	public static float EasyObstacleCount => easyObsCount;

	public static float HardObstacleCount => hardObsCount;

	public static int AdsCountDown => adsCountdown;

	public static float EmotionalDeathDuration => emotionalDeathDuration;
}
