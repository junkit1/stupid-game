using System;
using UnityEngine;

public enum ObstacleMovementType
{
    Forward, Diagonal
}

public class Obstacle : PoolObject
{
    private float passGap = 1.5f;
    private ObstacleBaseMovement obstacleMovement;
    private ObstacleData data;
    private float targetDuration = 0f;
    private float scaleY;

    public float TargetDuration => targetDuration;

	private void Awake()
	{
        scaleY = transform.localScale.y;
	}

	public void Initiate(ObstacleData obstacleData, float _targetDuration, Action<Obstacle> onCompleteAction, MovementEnhancer? enhancer = null)
    { 
        data = obstacleData;
        targetDuration = _targetDuration;
        obstacleMovement = obstacleData.MovementType switch
        {
            ObstacleMovementType.Forward => gameObject.AddComponent<ForwardMovement>(),
            ObstacleMovementType.Diagonal => gameObject.AddComponent<DiagonalMovement>(),
            _ => gameObject.AddComponent<ForwardMovement>()
        };
        SetupSpawnPos();
        obstacleMovement.assignOnFinish(() => { OnAnimationComplete(onCompleteAction); });
        gameObject.SetActive(true);
        obstacleMovement.InitiateMovement(_targetDuration, enhancer);
    }

    private void SetupSpawnPos()
	{
        float top = ScreenUtils.TopCap - (passGap * scaleY);
        float bottom = ScreenUtils.BottomCap + (passGap * scaleY);

        switch (data.MovementType)
		{
            case ObstacleMovementType.Diagonal:
                //spawn at top or bottom at 50% chance
                transform.position = new Vector3(transform.position.x, Utilis.FlipACoin() ? top : bottom, transform.position.z);
                break;
            default: transform.position = new Vector3(transform.position.x, UnityEngine.Random.Range(bottom, top), transform.position.z); break;
        }
    }

    protected override void ResetBeforeReturnPool()
	{
        obstacleMovement.Reset();
        Destroy(obstacleMovement);
	}

	private void OnAnimationComplete(Action<Obstacle> onCompleteAction)
	{
        onCompleteAction?.Invoke(this);
        StupidServiceLocator.GameManager.IncreaseScore();
        BackToPool();
	}

	public void Pause()
	{
        obstacleMovement.PauseMovement();
    }

    public void Resume()
	{
        obstacleMovement.ResumeMovement();
    }

}
