using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;


public enum MovementEnhancer
{
	SlowMiddle, FastMeddile, Reverse, SuddenSurge
}

public class ObstacleSpawner : MonoBehaviour
{
	[SerializeField]
	private ObstacleData[] obstacleDatas;

	private float offset = 0.5f;
	private int spawnedCount = 0;
	private List<Obstacle> allObstacles = new List<Obstacle>();
	private Coroutine spawnCoroutine;
	private float travelDuration = 0f;
	private WaitForSeconds spawnWait;
	private MovementEnhancer[] enhancerTypes = Enum.GetValues(typeof(MovementEnhancer)).Cast<MovementEnhancer>().ToArray();

	private void Start()
	{
		StupidServiceLocator.GameManager.SubscribeStartGameEvent(OnStartGame);
		StupidServiceLocator.GameManager.SubscribeIncreaseLevelEvent(IncreaseTravelDuration);
		StupidServiceLocator.GameManager.SubscribeNormalDeathEvent(StopEverything);
		StupidServiceLocator.GameManager.SubscribeEmotionalDeathEvent(StopEverything);
		StupidServiceLocator.GameManager.SubscribeRestartGameEvent(OnRestart);
		StupidServiceLocator.GameManager.SubscribeResetEvent(OnReset);
		transform.position = new Vector3(ScreenUtils.RightCap + offset, 0, 0);
	}

	private void Update()
	{
		//Testing
		if(Input.GetKeyDown(KeyCode.Space)){
			for (int i = 0; i < allObstacles.Count; i++)
			{
				if(Time.timeScale == 1)
				{
					allObstacles[i].Pause();
				}
				else
				{
					allObstacles[i].Resume();
				}
			}
			Time.timeScale = Time.timeScale == 0 ? 1 : 0;
		}
	}

	private void OnStartGame(object o, EventArgs e)
	{
		travelDuration = StupidGameSetting.ObstacleTravelDuration;
		spawnWait = new WaitForSeconds(travelDuration);
		spawnedCount = 0;
		SpawnObstacle();
	}

	public void InitiateSpawnCoroutine()
	{
		spawnCoroutine = StartCoroutine(RunSpawnCoroutine());
	}

	IEnumerator RunSpawnCoroutine()
	{
		yield return spawnWait;
		SpawnObstacle();
	}

	private void SpawnObstacle()
	{
		Obstacle obstacle = PoolUtility.RequestPoolObject<Obstacle>("Obstacle");
		ObstacleData data = obstacleDatas[UnityEngine.Random.Range(0, obstacleDatas.Length)];
		allObstacles.Add(obstacle);
		obstacle.transform.SetParent(gameObject.transform);
		obstacle.transform.position = gameObject.transform.position;
		MovementEnhancer? enhancer = null;
		if (RandomEnhancementChance())
		{
			enhancer = enhancerTypes[UnityEngine.Random.Range(0, enhancerTypes.Length)];
		}
		obstacle.Initiate(data, travelDuration, RemoveObstacleFromList, enhancer);
		if (StupidServiceLocator.GameManager.RequestCheckForDifficulty(++spawnedCount))
		{
			spawnedCount = 0;
		}
		InitiateSpawnCoroutine();
	}

	public void StopSpawn()
	{
		if(spawnCoroutine != null)
		{
			StopCoroutine(spawnCoroutine);
		}
	}

	private void IncreaseTravelDuration(object o, EventArgs args)
	{
		travelDuration = travelDuration > StupidGameSetting.MinObsTravelDuration ? travelDuration - StupidGameSetting.ObsTravelDurationDecrement : travelDuration;
		spawnWait = new WaitForSeconds(travelDuration);
	}

	public void RemoveObstacleFromList(Obstacle obs)
	{
		for (int i = 0; i < allObstacles.Count; i++)
		{
			if (allObstacles[i] == obs)
			{
				allObstacles.RemoveAt(i);
			}
		}
	}

	private bool RandomEnhancementChance()
	{
		float rng = UnityEngine.Random.Range(0, 1f);
		return rng < (StupidServiceLocator.GameManager.CurrentMode == Mode.Easy ? StupidGameSetting.EasyEnhancementChance : StupidGameSetting.HardEnhancementChance);
	}

	private void OnRestart(object o, EventArgs e)
	{
		Reset();
		SpawnObstacle();
	}

	private void OnReset(object o, EventArgs e)
	{
		Reset();
	}

	private void StopEverything(object o, EventArgs e)
	{
		StopSpawn();
		for (int i = 0; i < allObstacles.Count; i++)
		{
			allObstacles[i].Pause();
		}
	}

	private void Reset()
	{
		StopSpawn();
		foreach (Obstacle obs in allObstacles)
		{
			obs.BackToPool();
		}
		travelDuration = StupidGameSetting.ObstacleTravelDuration;
		spawnWait = new WaitForSeconds(travelDuration);
		spawnedCount = 0;
	}
}
