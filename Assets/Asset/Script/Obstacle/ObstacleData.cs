using UnityEngine;

[CreateAssetMenu(menuName = "ObstacleData")]
public class ObstacleData : ScriptableObject
{
	[SerializeField]
	private ObstacleMovementType movementType;

	public ObstacleMovementType MovementType => movementType;

}
