using System;
using UnityEngine;
using DG.Tweening;

public abstract class ObstacleBaseMovement : MonoBehaviour 
{
    private float offset = 0.5f;
    protected float leftEnd = 0;
    protected float rightEnd = 0;
    protected float targetDuration = 0; 
    protected Sequence movementSequence;
    protected Action onAnimationComplete;

	private void Awake()
	{
        leftEnd = ScreenUtils.LeftCap - offset;
        rightEnd = ScreenUtils.RightCap + offset;
    }

	public void InitiateMovement(float reachDuration, MovementEnhancer? enhancerType)
	{
        targetDuration = reachDuration;
        movementSequence = DOTween.Sequence();
        ActiveMovement(enhancerType);
    }

    protected abstract void ActiveMovement(MovementEnhancer? enhancerType);


    public abstract void Reset();


    public void assignOnFinish(Action onCompleteAction)
	{
        onAnimationComplete = onCompleteAction;

    }

	public void PauseMovement()
	{
        movementSequence.Pause();
    }

    public void ResumeMovement()
	{
        movementSequence.Play();
	}
}
