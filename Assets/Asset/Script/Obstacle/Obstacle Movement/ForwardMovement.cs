using DG.Tweening;
using UnityEngine;

public class ForwardMovement : ObstacleBaseMovement
{
	protected override void ActiveMovement(MovementEnhancer? enhancerType)
	{
		switch (enhancerType)
		{
			case MovementEnhancer.SlowMiddle:
				movementSequence.Append(transform.DOMoveX(0 / 2, targetDuration / 2).SetEase(Ease.OutSine))
				.Append(transform.DOMoveX(leftEnd, targetDuration / 2).SetEase(Ease.InSine));
				break;
			case MovementEnhancer.FastMeddile:
				movementSequence.Append(transform.DOMoveX(leftEnd, targetDuration).SetEase(Ease.InOutCubic));
				break;
			case MovementEnhancer.Reverse:
				movementSequence.Append(transform.DOMoveX(leftEnd, targetDuration).SetEase(Ease.Linear))
				.Append(transform.DOMoveX(rightEnd, targetDuration).SetEase(Ease.Linear));
				break;
			case MovementEnhancer.SuddenSurge:
				float ratio = 1 / Random.Range(2f, 6f);
				float ratioDuration = targetDuration * ratio;
				float remainingDuration = targetDuration - ratioDuration;
				float surgeDuration = Random.Range(remainingDuration / 2, remainingDuration / 5);
				float middleX = Mathf.Lerp(rightEnd, leftEnd, ratio);
				movementSequence.Append(transform.DOMoveX(middleX, ratioDuration).SetEase(Ease.Linear))
					.Append(transform.DOMoveX(leftEnd, surgeDuration).SetEase(Ease.Linear));
				break;
			default:
				movementSequence.Append(transform.DOMoveX(leftEnd, targetDuration).SetEase(Ease.Linear));
				break;
		}
		movementSequence.OnComplete(() => {
			onAnimationComplete.Invoke(); 
		});
		movementSequence.Play();
	}

    public override void Reset()
	{
        movementSequence?.Kill();
    }
}
