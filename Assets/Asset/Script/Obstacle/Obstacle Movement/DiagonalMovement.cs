using UnityEngine;
using DG.Tweening;

public class DiagonalMovement : ObstacleBaseMovement
{
	private float oriYPos = 0;
	private float endYPos = 0;

	protected override void ActiveMovement(MovementEnhancer? enhancerType)
	{
		endYPos = transform.position.y > 0 ? ScreenUtils.BottomCap : ScreenUtils.TopCap;
		switch (enhancerType)
		{
			case MovementEnhancer.SlowMiddle:
				movementSequence.Append(transform.DOMove(new Vector3(0, 0), targetDuration / 2).SetEase(Ease.OutSine))
				.Append(transform.DOMove(new Vector3(leftEnd, endYPos), targetDuration / 2).SetEase(Ease.InSine));
				break;
			case MovementEnhancer.FastMeddile:
				movementSequence.Append(transform.DOMove(new Vector3(leftEnd, endYPos), targetDuration).SetEase(Ease.InOutCubic));
				break;
			case MovementEnhancer.Reverse:
				oriYPos = transform.position.y;
				movementSequence.Append(transform.DOMove(new Vector3(leftEnd, endYPos), targetDuration).SetEase(Ease.Linear))
				.Append(transform.DOMove(new Vector3(rightEnd, oriYPos), targetDuration).SetEase(Ease.Linear));
				break;
			case MovementEnhancer.SuddenSurge:
				float ratio = 1 / Random.Range(2f, 6f);
				float ratioDuration = targetDuration * ratio;
				float remainingDuration = targetDuration - ratioDuration;
				float middleX = Mathf.Lerp(rightEnd, leftEnd, ratio);
				float middleY = Mathf.Lerp(transform.position.y, endYPos, ratio);
				movementSequence.Append(transform.DOMove(new Vector3(middleX, middleY), targetDuration * ratio).SetEase(Ease.Linear))
					.Append(transform.DOMove(new Vector3(leftEnd, endYPos), Random.Range(remainingDuration / 2, remainingDuration / 5)).SetEase(Ease.Linear));
				break;
			default:
				movementSequence.Append(transform.DOMove(new Vector3(leftEnd, endYPos), targetDuration).SetEase(Ease.Linear));
				break;
		}
		movementSequence.OnComplete(() => { onAnimationComplete.Invoke(); });
		movementSequence.Play();
	}

	public override void Reset()
	{
		movementSequence?.Kill();
	}
}
