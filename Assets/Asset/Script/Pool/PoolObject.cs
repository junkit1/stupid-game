using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PoolObject : MonoBehaviour
{
	private Queue<GameObject> pool;

	/// <summary>
	/// Just code what needs to be reset, don't need to be call manually cause it will be called inside BackToPool
	/// </summary>
	protected abstract void ResetBeforeReturnPool();

	public void RegisterPool(Queue<GameObject> q)
	{
		pool = q;
	}

	public void BackToPool()
	{
		gameObject.SetActive(false);
		ResetBeforeReturnPool();
		pool.Enqueue(gameObject);
	}
}
