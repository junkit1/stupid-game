public static class PoolUtility
{
    private static PoolManager poolManager;

    public static void RegisterPoolManager(PoolManager p)
	{
		if(poolManager == null)
		{
			poolManager = p;
		}
	}

	public static T RequestPoolObject<T>(string name)
	{
		var po = poolManager.GetPoolObject(name).GetComponent<T>();
		return po;
	}
}
