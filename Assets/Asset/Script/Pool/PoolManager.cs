using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    [SerializeField]
    private List<Pool> pools;

    private Dictionary<string, Queue<GameObject>> poolDictionary;

	private void Awake()
	{
        PoolUtility.RegisterPoolManager(this);
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        for (int i = 0; i < pools.Count; i++)
		{
            Queue<GameObject> queue = new Queue<GameObject>();
			for (int j = 0; j < pools[i].amount; j++)
			{
                GameObject g = Instantiate(pools[i].prefab);
                queue.Enqueue(g);
                g.SetActive(false);
			}
            poolDictionary.Add(pools[i].name, queue);
		}
	}

    public PoolObject GetPoolObject(string name) 
	{
		if (!poolDictionary.ContainsKey(name))
		{
            return null;
		}
        PoolObject g = null;
        if (poolDictionary[name].Count <= 0)
		{
			for (int i = 0; i < pools.Count; i++)
			{
                if(pools[i].name == name)
				{
                    g = Instantiate(pools[i].prefab).GetComponent<PoolObject>();
                    g.gameObject.SetActive(false);
                    break;
				}
			}
        }
		else
		{
            g = poolDictionary[name].Dequeue().GetComponent<PoolObject>();
		}
        g.RegisterPool(poolDictionary[name]);
        return g;
	}
}

[System.Serializable]
public class Pool
{
    public string name;
    public GameObject prefab;
    public int amount;
}
