using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private bool useKeyboard = false;
    [SerializeField]
    private PlayerDeathAnimation deathAnimation;
    private const float moveSpeed = 20f;
    private bool ableToMove = false;
    private float offset = 0.5f;
    private float topCap = 0;
    private float bottomCap = 0;
    private float leftCap = 0;
    private float rightCap = 0;

	private void Start()
	{
        float bodyHalfWidth = (GetComponent<Renderer>().bounds.size.x / 2);
        bottomCap = ScreenUtils.BottomCap + bodyHalfWidth + offset;
        topCap = ScreenUtils.TopCap - bodyHalfWidth - offset;
        leftCap = ScreenUtils.LeftCap + bodyHalfWidth + offset;
        rightCap = ScreenUtils.RightCap - bodyHalfWidth - offset;
        StupidServiceLocator.GameManager.SubscribeStartGameEvent(OnStartGame);
        StupidServiceLocator.GameManager.SubscribeRestartGameEvent(OnRestart);
        StupidServiceLocator.GameManager.SubscribeResetEvent(OnReset);
        StupidServiceLocator.GameManager.SubscribeEmotionalDeathEvent(OnEmotionalDamage);
    }

    void Update()
    {
        if (!ableToMove) return;
        var movement = useKeyboard ? new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized * (moveSpeed * Time.deltaTime) : StupidServiceLocator.GameManager.Input * (moveSpeed * Time.deltaTime);
        movement.x = ((movement.x < 0 && transform.position.x <= leftCap) || (movement.x > 0 && transform.position.x >= rightCap)) ? 0 : movement.x;
        movement.y = ((movement.y < 0 && transform.position.y <= bottomCap) || (movement.y > 0 && transform.position.y >= topCap)) ? 0 : movement.y;
        transform.position += new Vector3(movement.x , movement.y, 0);
    }

    private void OnStartGame(object o, EventArgs e)
	{
        EnableMovement();
    }

    private void OnReset(object o, EventArgs e)
    {
        Reset();
    }

    private void OnRestart(object o, EventArgs e)
	{
        Reset();
        EnableMovement();
    }

    private void OnEmotionalDamage(object o, EventArgs e)
	{
        DisableMovement();
        gameObject.SetActive(false);
        deathAnimation.PlayEmotionDeathAnimation(transform.position);
    }

    private void EnableMovement()
	{
        ableToMove = true;
	}

    private void DisableMovement()
	{
        ableToMove = false;
	}

    private void Reset()
	{
        gameObject.SetActive(true);
        transform.position = new Vector3(0, 0, 0);
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
		if (collision.CompareTag("Obstacle"))
		{
			DisableMovement();
			StupidServiceLocator.GameManager.TriggerNormalDeathEvent();
			gameObject.SetActive(false);
			deathAnimation.PlayNormalDeathAnimation(transform.position);
		}
	}
}
