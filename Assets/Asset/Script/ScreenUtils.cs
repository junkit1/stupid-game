using UnityEngine;

public static class ScreenUtils
{
	private static float aspect = (float)Screen.width / Screen.height;
	private static float worldHeight = Camera.main.orthographicSize * 2;
	private static float worldWidth = worldHeight * aspect;

	private static float pixelPerUnit = ((float)Screen.height / 2) / Camera.main.orthographicSize;
	private static float bottomCap = -Camera.main.orthographicSize + (Screen.safeArea.y / pixelPerUnit);
	private static float topCap = bottomCap + (Screen.safeArea.height / pixelPerUnit);
	private static float leftCap = (-worldWidth / 2) + (Screen.safeArea.x / pixelPerUnit);
	private static float rightCap = leftCap + (Screen.safeArea.width / pixelPerUnit);

	/// <summary>
	/// This return the screen overall height in unit without catering safe area
	/// </summary>
	public static float WorldHeight => worldHeight;

	/// <summary>
	/// this return screen overall width in unit without catering safe area
	/// </summary>
	public static float WorldWidth => worldWidth;

	public static float TopCap => topCap;

	public static float BottomCap => bottomCap;

	public static float LeftCap => leftCap;

	public static float RightCap => rightCap;

}
