using System;
using System.Collections;
using UnityEngine;

public enum Mode
{
    Easy, Hard
}

public enum DeathReason { Normal, Emotion };

public class DeathEventArgs: EventArgs
{
    public DeathReason deathReason = DeathReason.Normal;

    public DeathEventArgs(DeathReason r)
	{
        deathReason = r;
	}
}

public class GameManager : MonoBehaviour
{
    private Mode currentMode = Mode.Easy;

    private int score = 0;
    private int currentHighScore = 0;
    private const string highScoreKey = "highScore";
    private DeathReason deathReason;
    private Coroutine deathAnimationCoroutine;
    private WaitForSeconds emotionalDeathWait = new WaitForSeconds(StupidGameSetting.EmotionalDeathDuration);
    private WaitForSeconds normalDeathWait = new WaitForSeconds(0.5f);
    private event EventHandler startGameEvent;
    private event EventHandler increaseLevelEvent; 
    private event EventHandler resetEvent;
    private event EventHandler<DeathEventArgs> gameOverEvent;
    private event EventHandler restartGameEvent;
    private event EventHandler emotionalDeathEvent;
    private event EventHandler normalDeathEvent;

    public int Score => score;
    public int HighScore => currentHighScore;
    public Vector2 Input => StupidServiceLocator.UIManager.Joystick.Direction;

    public Mode CurrentMode => currentMode;

    private void Awake()
    {
        StupidServiceLocator.RegisterGameManager(this);
        currentHighScore = PlayerPrefs.GetInt(highScoreKey, 0);
    }

    public void TriggerStartGame()
    {
        startGameEvent?.Invoke(this, null);
    }

    public bool RequestCheckForDifficulty(int spawnedObsCount)
    {
        if (currentMode == Mode.Easy && spawnedObsCount >= StupidGameSetting.EasyObstacleCount)
        {
            currentMode = Mode.Hard;
            return true;
        }
        else if (currentMode == Mode.Hard && spawnedObsCount >= StupidGameSetting.HardObstacleCount)
        {
            currentMode = Mode.Easy;
            TriggerIncreaseLevel();
            return true;
        }
        return false;
    }

    public void IncreaseScore()
    {
        score++;
        StupidServiceLocator.UIManager.UpdateScoreUI(score);
    }

    private void TriggerIncreaseLevel()
    {
        increaseLevelEvent?.Invoke(this, null);
    }

    public void SubscribeStartGameEvent(EventHandler e)
    {
        startGameEvent += e;
    }

    public void SubscribeIncreaseLevelEvent(EventHandler e)
    {
        increaseLevelEvent += e;
    }

    public void SubscribeEmotionalDeathEvent(EventHandler e)
    {
        emotionalDeathEvent += e;
    }

    public void SubscribeNormalDeathEvent(EventHandler e)
	{
        normalDeathEvent += e;
	}

    public void SubscribeGameOverEvent(EventHandler<DeathEventArgs> e)
    {
        gameOverEvent += e;
    }

    public void SubscribeResetEvent(EventHandler e)
    {
        resetEvent += e;
    }

    public void SubscribeRestartGameEvent(EventHandler e)
    {
        restartGameEvent += e;
    }

    public void TriggerResetEvent()
    {
        Reset();
        resetEvent?.Invoke(this, null);
    }

    public void TriggerRestartEvent()
    {
        Reset();
        restartGameEvent?.Invoke(this, null);
    }

    IEnumerator EmotionalDamageDelay()
    {
        yield return emotionalDeathWait;
        TriggerGameOver();
    }

    IEnumerator NormalDeathDelay()
    {
        yield return normalDeathWait;
        TriggerGameOver();
    }


    public void TriggerEmotionalDamageEvent()
	{
        emotionalDeathEvent?.Invoke(this, null);
        deathReason = DeathReason.Emotion;
        if (deathAnimationCoroutine != null)
        {
            StopCoroutine(deathAnimationCoroutine);
        }
        deathAnimationCoroutine = StartCoroutine(EmotionalDamageDelay());
    }

    public void TriggerNormalDeathEvent()
	{
        normalDeathEvent?.Invoke(this, null);
        deathReason = DeathReason.Normal;
        if (deathAnimationCoroutine != null)
        {
            StopCoroutine(deathAnimationCoroutine);
        }
        deathAnimationCoroutine = StartCoroutine(NormalDeathDelay());
    }

    private void TriggerGameOver()
	{
        if(score > currentHighScore)
		{
            currentHighScore = score;
            PlayerPrefs.SetInt(highScoreKey, currentHighScore);
		}
        currentHighScore = score > currentHighScore ? score : currentHighScore;
        gameOverEvent?.Invoke(this, new DeathEventArgs(deathReason));
    }

	private void Reset()
	{
        score = 0;
        currentMode = Mode.Easy;
    }

    public void RestartGame()
	{
        restartGameEvent?.Invoke(this, null);
    }
}
