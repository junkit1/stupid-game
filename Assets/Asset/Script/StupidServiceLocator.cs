public static class StupidServiceLocator
{
	private static GameManager gameManager;

	private static UIManager uiManager;

	private static UnityAdsManager adsManager;

	public static GameManager GameManager => gameManager;

	public static UIManager UIManager => uiManager;

	public static UnityAdsManager AdsManager => adsManager;

	public static void RegisterGameManager(GameManager g)
	{
		if (gameManager == null)
		{
			gameManager = g;
		}
	}

	public static void RegisterUIManager(UIManager u)
	{
		if (uiManager == null)
		{
			uiManager = u;
		}
	}

	public static void RegisterAdsManager(UnityAdsManager u)
	{
		if(adsManager == null)
		{
			adsManager = u;
		}
	}
}