using UnityEngine.Advertisements;
using UnityEngine;

public class UnityAdsManager : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsShowListener, IUnityAdsLoadListener
{
	private const string andriodGameID = "5149834";
	private const string interstitialVideo = "Interstitial_Android";
	private bool testMode = false;
	private bool loaded = false;

	public bool AdsLoaded => loaded;

	// Start is called before the first frame update
	void Awake()
    {
		StupidServiceLocator.RegisterAdsManager(this);
		Initialise();
	}

	private void Initialise()
	{
		Advertisement.Initialize(andriodGameID, testMode, this);
	}

	public void OnInitializationComplete()
	{
		LoadAd();
	}

	public void OnInitializationFailed(UnityAdsInitializationError error, string message)
	{
		StupidServiceLocator.UIManager.ShowGameOverPopUp();
	}

	public void LoadAd()
	{
		// IMPORTANT! Only load content AFTER initialization (in this example, initialization is handled in a different script).
		Advertisement.Load(interstitialVideo, this);
	}

	public void ShowAd()
	{
		// Note that if the ad content wasn't previously loaded, this method will fail
		if (loaded)
		{
			Advertisement.Show(interstitialVideo, this);
		}
	}

	public void OnUnityAdsShowFailure(string placementId, UnityAdsShowError error, string message)
	{
		StupidServiceLocator.UIManager.ShowGameOverPopUp();
		loaded = false;
		LoadAd();
	}

	public void OnUnityAdsShowStart(string placementId){}

	public void OnUnityAdsShowClick(string placementId){}

	public void OnUnityAdsShowComplete(string placementId, UnityAdsShowCompletionState showCompletionState)
	{
		StupidServiceLocator.UIManager.ShowGameOverPopUp();
		loaded = false;
		LoadAd();
	}

	public void OnUnityAdsAdLoaded(string placementId)
	{
		loaded = true;
	}

	public void OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
	{
		loaded = false;
	}
}
