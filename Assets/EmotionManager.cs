using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionManager : MonoBehaviour
{
    private Coroutine emotionTimeBomb;
    private WaitForSeconds timer = new WaitForSeconds(30);
	private float emotionDamageChance = 0.05f;

	private void Start()
	{
		StupidServiceLocator.GameManager.SubscribeStartGameEvent(StartTimeBomb);
		StupidServiceLocator.GameManager.SubscribeRestartGameEvent(StartTimeBomb);
		StupidServiceLocator.GameManager.SubscribeNormalDeathEvent(StopTimeBomb);
	}

	private void TriggerTimeBomb()
	{
		if (emotionTimeBomb != null)
		{
			StopCoroutine(emotionTimeBomb);
		}
		emotionTimeBomb = StartCoroutine(TimerCountDown());
	}

	private void StartTimeBomb(object o, EventArgs e)
	{
		TriggerTimeBomb();
	} 

	IEnumerator TimerCountDown()
	{
		yield return timer;
		RandomEmotionChance();
	}

	private void StopTimeBomb(object o, EventArgs e)
	{
		if (emotionTimeBomb != null)
		{
			StopCoroutine(emotionTimeBomb);
		}
	}

	private void RandomEmotionChance()
	{
		float odd = UnityEngine.Random.Range(0, 1f);
		if (odd < emotionDamageChance)
		{
			StupidServiceLocator.GameManager.TriggerEmotionalDamageEvent();
		}
		else
		{
			TriggerTimeBomb();
		}
	}
}
